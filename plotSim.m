function h = plotSim(sim)
    
    h = figure; 
    set(h,'color','w');
        
    subplot(2,2,[1,2]);
    
    hold on    
    grid on
    
    plot(sim.position(:,1), sim.position(:,2), '-o');
    
    originalYrefs = sim.yrefs(sim.originalYrefs == 1, :);
    plot(originalYrefs(:,1), originalYrefs(:,2), '^r');
    
    for i = 1 : size(sim.bypassedObjects, 1)
        [polygonX, polygonY] = getPolygon(sim.bypassedObjects(i, :));
        fill(polygonX, polygonY, [0.5, 0.5, 0.5]);
    end
    
    xlabel('Abscissa (m)');
    ylabel('Ordenada (m)');
    title(sprintf('Movimentação segundo o caminho %s', mat2str(originalYrefs)));
    
    subPlotSim(3, 'x(k)', sim.timeVSkplusOne);
    subPlotSim(4, 'deltaU(k)', sim.timeVSdeltaUk);
    
    x0=100;
    y0=100;
    width=600;
    height=600
    set(h,'units','points','position',[x0,y0,width,height])
    
end

function subPlotSim(i, label, timeseries)
    
        subplot(2,2,i);
        time = timeseries(:,1);
        y1 = timeseries(:,2);
        y2 = timeseries(:,3);        
        plot(time, y1, time, y2);
        legend(sprintf('%s-1', label), sprintf('%s-2', label));
        grid minor
        xlabel('Tempo (s)');
        ylabel(sprintf('%s', label));
    
end