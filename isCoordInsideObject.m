function inside = isCoordInsideObject(coord, object)
    global radius
    
    [polygonX, polygonY] = getPolygon(object);
    
    inside = inpolygon(coord(1) + radius, coord(2) + radius, polygonX, polygonY) || ...
             inpolygon(coord(1) + radius, coord(2) - radius, polygonX, polygonY) || ...
             inpolygon(coord(1) - radius, coord(2) - radius, polygonX, polygonY) || ...
             inpolygon(coord(1) - radius, coord(2) + radius, polygonX, polygonY);
end