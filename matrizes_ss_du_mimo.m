function matrizes_ss_du_mimo()

    global Phi G Gn Hqp Aqp A B C p q n N M mu vmax amax rho bqp T xzero 
    
    n = size(A,1);
    p = size(B,2);
    q = size(C,1);
    
    At = [A B;zeros(p,n) eye(p)];
    Bt = [B;eye(p)];
    Ct = [C zeros(q,p)];

    G = zeros(q*N,p*M);
    for i = 1:N
       for j = 1:min(i,M)
           G(1+(i-1)*q:i*q , 1+(j-1)*p:j*p) = Ct*(At^(i-j))*Bt;
       end
    end

    Phi = Ct*At;
    for i = 2:N
        Phi = [Phi; Ct*At^i];
    end
    
    muRep  = repmat(mu,  size(xzero, 1) / 2, 1);
    rhoRep = repmat(rho, size(xzero, 1) / 2, 1);
    
    Q = diag(muRep);
    R = diag(rhoRep);
    Qbar = Q;
    for i = 2:N
        Qbar = blkdiag(Qbar,Q);
    end
    Rbar = R;
    for j = 2:M
        Rbar = blkdiag(Rbar,R);
    end
    
    %slide Aula 9 slide 23
    Gn = Qbar*G;
    Hqp = 2*(G'*Qbar*G + Rbar);
    Hqp = 0.5 * (Hqp + Hqp');
    Aqp = eye(p*M);
    Aqp = [Aqp; -Aqp];
    
    axialSpeed = vmax * sqrt(2)/2;
    accelerationTime = axialSpeed/amax;
  
    if(accelerationTime < T)        
        deltaUmax = axialSpeed * (T - accelerationTime);
    else
        axialSpeed = amax * T;
        deltaUmax = 0.5 * axialSpeed;
    end
    
    bqp = repmat(deltaUmax, 2*p*M, 1);
    
%     if(fieldLimitsOn)
%         error('oi');
%     end
end