function [sys,dummy0,str,ts] = mpc_ss_du_mimo(t,~,inputs,flag)

    global Gn Phi p q n N T Hqp Aqp bqp
    
% Parâmetros de entrada:
% KMPC: Vetor de ganho (p x qN)
% Phi: Vetor coluna (qN x (n+p)) --> [CA;CA^2; ... ; CA^N]
% p: Número de variáveis manipuladas
% q: Número de variáveis controladas
% n: Número de estados da planta
% N: Horizonte de predição
%
% T: Período de amostragem
%
% Variáveis de entrada (nesta ordem): x(k), u(k-1)
%
% Autor: Roberto Kawakami Harrop Galvao
% ITA - Divisao de Engenharia Eletronica
% Data: 11 de abril de 2017
% Adaptado por: Israel Cordeiro Rocha
% Data: junho de 2017

    switch flag
        case 0
        [sys,dummy0,str,ts] = mdlInitializeSizes(T,p,q,n); % S-function Initialization
         
        case 2
        sys = mdlUpdate(t);        

        case {1, 4, 9}
        sys = []; % Unused Flags

        case 3 % Evaluate Function  

        xk = inputs(1 : n); % x(k)
        yref = inputs(n + 1 : n + q);
        ukMinusOne = inputs(n + q + 1 : end - n);  % u(k-1)
        xkMinusOne = inputs(end - n + 1 : end);
        
                
        if(min(xkMinusOne == xk))
            compensate = ukMinusOne;
            ukMinusOne = zeros(p,1);
        else
            compensate = zeros(p,1);
        end
                
        xki = [xk; ukMinusOne];

        % Valores futuros de referência
        r = repmat(yref, N, 1);

        % Resposta livre
        f = Phi*xki;
        fqp = 2 * Gn'*(f-r); 
             
        duk = quadprog(Hqp, fqp, Aqp, bqp);
        sys =  duk(1 : p); % S-function output (incremento no controle)
        sys = sys - compensate;
      
    end

end

%%%%%%%%%%%%%%%%%%%%%%%%

%=============================================================================
% mdlInitializeSizes
% Return the sizes, initial conditions, and sample times for the S-function.
%=============================================================================
%
function [sys,dummy0,str,ts] = mdlInitializeSizes(T,p,q,n)

%
% call simsizes for a sizes structure, fill it in and convert it to a
% sizes array.
%
%
sizes = simsizes;

sizes.NumContStates  = 0;
sizes.NumDiscStates  = 0;
sizes.NumOutputs     = p; % du(k)
sizes.NumInputs      = n + q + p + n; % x(k), yref, u(k-1), x(k-1)
sizes.DirFeedthrough = 1; % Yes
sizes.NumSampleTimes = 1;   % Just one sample time

sys = simsizes(sizes);

%
% initialize the initial conditions
%
dummy0  = [];

%
% str is always an empty matrix
%
str = [];

%
% initialize the array of sample times
%
ts  = [T 0];

end

%=======================================================================
% mdlUpdate
% Handle discrete state updates, sample time hits, and major time step
% requirements.
%=======================================================================
%
function sys = mdlUpdate(~)

sys = [];

end