function visible = canRobotSeeNearYrefTarget(object, xk)
    global index sim radius
        
    target = sim.yrefs(index, :);
    
    distance = sqrt(sum((target' - xk).^2));
    
    if(distance > 10 * radius)
        visible = false;
        return;
    end
        
    
    [polygonX, polygonY] = getPolygon(object);
    
    xmax = max(polygonX(1));
    ymax = max(polygonY(2));
    xmin = min(polygonX(1));
    ymin = min(polygonY(2));
    
    
    line = [xk(1), xk(2), target(1), target(2)];
    edge1 = [xmax, ymax, xmax, ymin];
    edge2 = [xmin, ymax, xmax, ymax];
    edge3 = [xmin, ymin, xmax, ymin];
    edge4 = [xmin, ymin, xmin, ymax];
    
    visible = isnan(lineintersect(line, edge1)) || ...
              isnan(lineintersect(line, edge2)) || ...
              isnan(lineintersect(line, edge3)) || ...
              isnan(lineintersect(line, edge4));

end
