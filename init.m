global T vmax amax rho mu N M Ac Bc C A B D xzero Range radius dangerDetection

% all measures in metric-second system.

xzero = [-5; 10]; % initial robot's position
Range = 20;  %range of robot's antenna
radius = 0.3; % radius of robot's circular body
vmax = 10; %m/s
amax = 5;  %m/s^2
dangerDetection = true;

T = 0.5; %simulation step
rho = [1; 1];
mu  = [1; 1];
N = 15;
M = 10;


Ac = zeros(size(xzero, 1));
Bc = eye(size(xzero, 1));
C = eye(size(xzero, 1));
D = zeros(size(xzero, 1));
[A, B] = c2dm(Ac,Bc,C,D,T,'zoh');