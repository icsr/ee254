function [sys,dummy0,str,ts] = seekObstacles(t,~,inputs,flag)

    global T objectsDetected dangerDetection
    
    switch flag
        case 0
            [sys,dummy0,str,ts] = mdlInitializeSizes(T); % S-function Initialization
                       
            if(dangerDetection)
                objectsDetected = [0, 0,  5, 5; 15, 15, 22, 22];
            else
                objectsDetected = [];
            end        
                

%         case 2
%             sys = mdlUpdate(t);      
        case {1, 2, 3, 4, 9}
            sys = []; % Unused Flags
    end        

end

%%%%%%%%%%%%%%%%%%%%%%%%

%=============================================================================
% mdlInitializeSizes
% Return the sizes, initial conditions, and sample times for the S-function.
%=============================================================================
%
function [sys,dummy0,str,ts] = mdlInitializeSizes(T)

%
% call simsizes for a sizes structure, fill it in and convert it to a
% sizes array.
%
%
sizes = simsizes;

sizes.NumContStates  = 0;
sizes.NumDiscStates  = 0;
sizes.NumOutputs     = 0; % [yref, xk, dangerDetected]
sizes.NumInputs      = 1; % [u(k), x(k), objectsDetected]
sizes.DirFeedthrough = 1; % Yes
sizes.NumSampleTimes = 1;   % Just one sample time

sys = simsizes(sizes);

%
% initialize the initial conditions
%
dummy0  = [];

%
% str is always an empty matrix
%
str = [];

%
% initialize the array of sample times
%
ts  = [T 0];

end

%=======================================================================
% mdlUpdate
% Handle discrete state updates, sample time hits, and major time step
% requirements.
%=======================================================================
%
function sys = mdlUpdate(~)

sys = [];

end