function [polygonX, polygonY] = getPolygon(object)

    xmin = min(object(1), object(3)); 
    xmax = max(object(1), object(3));
    ymin = min(object(2), object(4)); 
    ymax = max(object(2), object(4));
    
    polygonX = [xmin, xmax, xmax, xmin];
    polygonY = [ymin, ymin, ymax, ymax];


end