clear global;
init

global yrefs dangerDetection

yrefs = [-10, 0; -5, 4; 10, 4; 15, 0];
dangerDetection = false; sim('mpc_ss_du');
dangerDetection = true; sim('mpc_ss_du');


yrefs = [5, 0; -5, 0; 10, 0; 15, 0];
dangerDetection = false; sim('mpc_ss_du');
dangerDetection = true; sim('mpc_ss_du');

yrefs = [15, 10; 10, 10; 5, 5; 15, 15; 20, 20];
dangerDetection = false; sim('mpc_ss_du');
dangerDetection = true; sim('mpc_ss_du');

yrefs = [15, 10; 10, 10; 5, 5; 15, 15; 25, 25];
dangerDetection = false; sim('mpc_ss_du');
dangerDetection = true; sim('mpc_ss_du');

clc
close all