function inside = isCoordInsideRobot(coord, xk)
    global radius
    
    polygonX = [xk(1) + radius, xk(1) + radius, xk(1) - radius, xk(1) - radius]; 
    polygonY = [xk(2) + radius, xk(2) - radius, xk(2) - radius, xk(2) + radius] ;
        
    inside = inpolygon(coord(1), coord(2), polygonX, polygonY);
end