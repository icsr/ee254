function [sys,dummy0,str,ts] = robotDynamic(t,~,inputs,flag)

    global p q n T simTime sim xzero objectsDetected filenameId
    
    simTime = t;
    
    switch flag
        case 0
            [sys,dummy0,str,ts] = mdlInitializeSizes(T+0.01,p,q,n); % S-function Initialization
            dummyInit(); 
            sim.position = [];
            sim.bypassedObjects = [];
            sim.originalYrefs = ones(size(sim.yrefs, 1), 1);
            filenameId = mat2str(sim.yrefs);
        case 2
            sys = mdlUpdate(t);      
        case {1, 4}
            sys = []; % Unused Flags

        case 3 % Evaluate Function  
            
            uk = inputs(1 : p);
            xk = inputs(p+1 : p + n);            
            
            [yref, xkPlusOne, dangerDetected] = evaluate(uk, xk);            
            sim.position = [sim.position; xkPlusOne'];            
            sys = [yref; xkPlusOne; dangerDetected];
            
        case 9
            sim.originalYrefs = [true;  sim.originalYrefs];
            sim.yrefs = [xzero'; sim.yrefs];
            
            load('tmp/tkplusone.mat');
            sim.timeVSkplusOne = value';
            load('tmp/tdeltauk.mat');
            sim.timeVSdeltaUk = value';
                       
            
            dangerDetection = ~isempty(objectsDetected);
            
            file = sprintf('%s/%s_dd%d', 'resultados', filenameId, dangerDetection);            
            save([file '.mat'], 'sim');
            
            h = plotSim(sim);
            saveas(h, [file '.fig']);
            print([file '.png'], '-dpng');

    end

end

%%%%%%%%%%%%%%%%%%%%%%%%

%=============================================================================
% mdlInitializeSizes
% Return the sizes, initial conditions, and sample times for the S-function.
%=============================================================================
%
function [sys,dummy0,str,ts] = mdlInitializeSizes(T,p,q,n)

%
% call simsizes for a sizes structure, fill it in and convert it to a
% sizes array.
%
%
sizes = simsizes;

sizes.NumContStates  = 0;
sizes.NumDiscStates  = 0;
sizes.NumOutputs     = q + n + 1; % [yref, xk, dangerDetected]
sizes.NumInputs      = p + n; % [u(k), x(k)]
sizes.DirFeedthrough = 1; % Yes
sizes.NumSampleTimes = 1;   % Just one sample time

sys = simsizes(sizes);

%
% initialize the initial conditions
%
dummy0  = [];

%
% str is always an empty matrix
%
str = [];

%
% initialize the array of sample times
%
ts  = [T 0];

end

%=======================================================================
% mdlUpdate
% Handle discrete state updates, sample time hits, and major time step
% requirements.
%=======================================================================
%
function sys = mdlUpdate(~)

sys = [];

end

function [yref, xkPlusOne, dangerDetected] = evaluate(uk, xk)                  
    global index sim

    xktemp = xk + uk;
    [dangerDetected, object] = checkDanger(xktemp);

    if(~dangerDetected)            
        xkPlusOne = xktemp;        
    else
        xkPlusOne = xk;
        if(sim.originalYrefs(index))
            if(canRobotSeeNearYrefTarget(object, xk))
                goToNeighboorhood(object);
            else
                circunvetObject(object, xk);                       
            end
        end
    end

    
    if(isCoordInsideRobot(xkPlusOne, sim.yrefs(index, :)))        
        %chegou e vai para o próximo
        index = min(index + 1, size(sim.yrefs,1));
    end        

    [alert, object] = checkDanger(sim.yrefs(index, :));

    if(alert)        
        goToNeighboorhood(object);               
    end
    
    yref = sim.yrefs(index, :)';
end

function goToNeighboorhood(object)
    global radius sim index
    
    yref = sim.yrefs(index, :);
    
    overRadius = 1.1*radius;
        
    [polygonX, polygonY] = getPolygon(object);
    xmin = min(polygonX);
    xmax = max(polygonX);
    ymin = min(polygonY);
    ymax = max(polygonY);
    
    distances = [
                abs(yref(1) - xmin);
                abs(yref(1) - xmax);
                abs(yref(2) - ymin);
                abs(yref(2) - ymax);
                ];
    
     k = find(distances == min(distances));
          
     switch k(1)
         case 1             
             yref(1) = yref(1) - distances(1) - overRadius;
         case 2
             yref(1) = yref(1) + distances(2) + overRadius;
         case 3
             yref(2) = yref(2) - distances(3) - overRadius;
         case 4
             yref(2) = yref(2) + distances(4) + overRadius;
     end
     
     sim.yrefs(index, :) = yref';

end

function circunvetObject(object, xk)
    global sim index radius
        
    overRadius = 1.1*radius;
    
    [polygonX, polygonY] = getPolygon(object);
    
    xmax = max(polygonX) + overRadius;
    ymin = min(polygonY) - overRadius; 
    ymax = max(polygonY) + overRadius;
            
    if(abs(xk(2) - ymin) <= abs(xk(2) - ymax))
        waypoints = [xk(1), ymin; xmax, ymin];
    else
        waypoints = [xk(1), ymax; xmax, ymax];
    end
    
    if(isnan(max(waypoints)))
        return;
    end
        
    waypoints = [waypoints; xmax, xk(2); xmax, sim.yrefs(index, 2)];
    notOriginal = zeros(size(waypoints,1), 1);
    
    if(index == 1)
        sim.yrefs = [waypoints; sim.yrefs];
        sim.originalYrefs = [notOriginal; sim.originalYrefs];
    else
        sim.yrefs = [sim.yrefs(1 : index - 1, :); waypoints; sim.yrefs(index : end, :) ];
        sim.originalYrefs = [sim.originalYrefs(1 : index - 1, :); notOriginal; sim.originalYrefs(index : end, :) ];
    end
    
end

function [alert, dangerObject] = checkDanger(xktemp)
    global objectsDetected sim
    
    alert = false;
    dangerObject = [];
    if(isempty(objectsDetected))
        return;
    end
    
    nobjs = size(objectsDetected, 1);
    
    for i = 1 : nobjs
        object = objectsDetected(i, :);       
        if(isCoordInsideObject(xktemp, object))
            alert = true;
            dangerObject = object;
            sim.bypassedObjects = [sim.bypassedObjects; object];
            return;
        end
    end

end

function dummyInit()
    global sim index yrefs
    if(isempty(yrefs))
        yrefs = [-10, 0; -5, 4; 10, 4; 15, 0];
    end
    sim.yrefs = yrefs;
    index = 1;    
end